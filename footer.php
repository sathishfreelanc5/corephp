<section id="footer-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-3"  data-aos="fade-left" data-aos-easing="linear" data-aos-duration="500" data-aos-once="true">
                <div class="d-flex justify-content-center" id="header-menu1">
                    <a class="navbar-brand" href="#"><h1 id="logo-fontfamilly" style="color: green;"><span  style="color: #db0000f0;">snine</span>starz</span></h1></a>
                </div>
                <div class="d-flex justify-content-center pt-3" id="header-menu1">   
                    <p class="text-muted">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a consequat nibh. 
                        Praesent sodales enim facilisis accumsan rhoncus.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </p>
                </div>
            </div>
            <div class="col-md-4" data-aos="fade-left" data-aos-easing="linear" data-aos-duration="500" data-aos-once="true">
                <div class="d-flex justify-content-center" id="header-menu1">
                   <h6 style="color: white;font-weight:bold;">CONTACT ADDRESS</h6>
                    <div class="line-5"></div>
                    <div class="line-6"></div>
                </div>
                <div class="d-flex justify-content-center" id="header-menu1">
                    <address style="color: white;font-weight:bold;padding-top:30px;padding-left:30px;">
                        Jonsson Street 123/80 Road,<br>
                        Mans Diego's city, California,<br>
                        United States 10111 Santa Monica Boulevard.
                    </address>
                </div>
                <div class="d-flex justify-content-center" id="header-menu1">
                    <address style="color: white;font-weight:bold;padding-top:4px;padding-right:110px;">
                        (+852 ) 123-4567-890
                    </address>
                </div>
                <div class="d-flex justify-content-center" id="header-menu1">
                    <address style="color: white;font-weight:bold;padding-top:4px;padding-right:110px;">
                        info@yourdomain.com
                    </address>
                </div>
            </div>
            <div class="col-md-3" data-aos="fade-right" data-aos-easing="linear" data-aos-duration="500" data-aos-once="true">
                <div class="d-flex justify-content-center" id="header-menu2">
                    <h6  style="color: white;font-weight:bold;">USEFULL LINKS</h6>
                </div>
                <div class="d-flex justify-content-center" id="header-menu2">
                    <address style="color: white;font-weight:bold;padding-top:30px;padding-right:0px;">
                        <h6 class="text-center">Artist</h6>
                    </address>
                </div>
                <div class="d-flex justify-content-center" id="header-menu2">
                    <address style="color: white;font-weight:bold;padding-top:4px;padding-right:0px;">
                        <h6 class="text-center">Technitions</h6>
                    </address>
                </div>
                <div class="d-flex justify-content-center" id="header-menu2">
                    <address style="color: white;font-weight:bold;padding-top:4px;padding-right:0px;">
                        <h6 class="text-center">Advertisement</h6>
                    </address>
                </div>
                <div class="d-flex justify-content-center" id="header-menu2">
                    <address style="color:white;font-weight:bold;padding-top:4px;padding-right:0px;">
                        <h6 class="text-center">Models</h6>
                    </address>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-right" data-aos-easing="linear" data-aos-duration="500" data-aos-once="true">
                <div class="d-flex justify-content-center" id="header-menu2">
                    <h6  style="color: white;font-weight:bold;">SOCIAL LINKS</h6>
                </div>
                <div class="d-flex justify-content-center" id="header-menu2">
                   <div  style="color:white;font-weight:bold;padding-top:28px;padding-right:0px;font-size:30px;">
                      <i class="fa fa-facebook-square" style="color: white;" aria-hidden="true"></i>
                   </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="footer-menu-1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="text-center pt-3 "><a href="https://mediawonderz.com/" target="_blank" style="text-decoration: none;color:white;">@Copyright Design By Media Wonderz-2022</a></p>
            </div>
        </div>
    </div>
</section>